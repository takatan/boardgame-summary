# The Resistance

## Plot Thickens

* 5-6人のときは7枚。7人以上のときは15枚。
* カードは公開情報
* ラウンドの開始時にリーダーがカードを引く
  * 5-6→1 7-8→2 9-10→3
  * 引いたカードを他のプレイヤーに配布する(自分は除く)
* 1は一回のみ。★はすぐ使う。■は永続。
* リーダーに近い順に解決する

## Hidden Agenda

### Assassin Module

* AssassinとCommander等が入る

### Trapper Module

* 規定のチームメンバーより一人多く指名する
* チームが承認されたらミッションカードを提出してもらう。そのうち一枚の中身を確認して破棄する。公開はしない。残りのカードでミッションの判定を行う。

### Defector その1

* 両方のDefectorを通常のカードと交換して配布する。
* Loyaltryデッキを5枚セットする。
* スパイ確認のときにDefectorは目を開けない。替わりに手を上げる。
* 第三ラウンドと以後のラウンドの開始前にLoyaltyデッキをオープンする。
* Switch Allegianceが出たらDefectorの陣営は入れ替わる

### Defector その2

* Defectorはお互いを確認する。
* 開始前に確認するフェイズを入れる。

## Hostile Intent

### Hunter Module

### Inquisitor Module

* 最初のリーダーの右隣に置く
* 2R・3R・4Rのミッション終了後に使う
* 対象プレーヤーが陣営を教える
* Inquisitorトークンは対象に移動する

### Reverse Module

* 通常のレジスタンスとスパイに替えてReverserを使う
* Spy ReverserはSuccessとReverseだけ使う(Failは使えない)。
* オプションとしてReverserはSpyの確認フェイズで出ずにやる

## Promos

## Rogue Agent Module

第三陣営

## Sergent Module

* Sergent はリーダーがチームを指名するときに投票前にチームメンバー一人
  を指名する。リーダーはSergentを兼任できない。
* ミッションカードを決定したあとSergentはチームメンバーの一人を指名で
  きる。指名されたメンバーはミッションカードを交換する。
* Sergentは交換されたカードの中身を確認する。
* その後カードを集めて通常通りに判定を行う。
