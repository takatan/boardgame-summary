# Quo Vadis?

## 行動

* 入場
* 昇進(委員会の定数の過半数の賛成)
* カエサルトークンの移動(何もできなけない場合はこれをする)

## 注意

* 他人の賛成に1点(可決に最低限の点数)
* 交渉したことは自分のターン中は守る
* カエサルの上はフリーパス。ただし点数トークンは取れない。
* 元老院の議席数は順位に関係ない(順序はタイブレーク)

## 終了

* 元老院が5席埋まったら即座にゲーム終了
* 議席がないと0点
* 同点の場合は早く元老院に到達した人の勝ち
