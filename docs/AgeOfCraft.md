# Age of Craft 大建築時代

## 準備

* 各自ダイスを3個振る
* トップ(同点は振り直し)から交換かパス。

## ターン

* 3個振る
* 全員バーストチェックする。(9個なら8以下が出たらバースト)
  * バーストしたら大きい目から供出
* ターンプレーヤーから1個ずつダイスを獲得
* 交渉
* 建築…何枚でも可。

## 終了

* 誰かが20点
* 大商館が売り切れ
トップ同点の場合は同点一位

## 注意

同じ目のダイスの交換は交渉ではない
